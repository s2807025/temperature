package nl.utwente.di.fahrenheit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;




public class TestDegree {

    @Test
    public void testBook1() throws Exception{
        degree degree = new degree();
        double celsius = degree.getFahrenheit("10");
        Assertions.assertEquals(50.0, celsius);
    }
}
